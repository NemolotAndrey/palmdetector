package com.mx.krakensoft.opencv;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfInt;
import org.opencv.core.MatOfInt4;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

import com.mx.krakensoft.opencv.imageProcessing.ColorBlobDetector;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.core.Size;

public class MainActivity extends Activity implements OnTouchListener, CvCameraViewListener2 {

    static {
        System.loadLibrary("opencv_java3");
    }

    private static final String TAG = "HandPose::MainActivity";
    final Handler mHandler = new Handler();

    private Mat mRgba;

    private CustomSufaceView mOpenCvCameraView;
    private ImageView imageCapture;

    private ColorBlobDetector mColorBlobDetector;
    private Mat mSpectrum;
    private boolean mIsColorSelected = false;

    private Size SPECTRUM_SIZE;
    private Scalar CONTOUR_COLOR;
    private int numberOfFingers = 0;
    Bitmap capturedPhoto;

    private boolean isPhotoMaked = false;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i(TAG, "OpenCV loaded successfully");
                    mOpenCvCameraView.enableView();
                    mOpenCvCameraView.setOnTouchListener(MainActivity.this);
//                    mOpenCvCameraView.setMaxFrameSize(640, 480);
                    // 640x480
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    public MainActivity() {
        Log.i(TAG, "Instantiated new " + this.getClass());
    }

    public void btnMakeCapture(View view) {
//        makeCapture()
        setDetectPoint();
    }

    private void makeCapture() {

        capturedPhoto = Bitmap.createBitmap(mRgba.cols(), mRgba.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(mRgba, capturedPhoto);
//            if (mOpenCvCameraView!= null) mOpenCvCameraView.disableView();
//            savebitmap(capturedPhoto);
        mHandler.post(mUpdateFingerCountResults);
        Camera.Parameters cParams = mOpenCvCameraView.getParameters();
        cParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        mOpenCvCameraView.setParameters(cParams);

//            mOpenCvCameraView.setVisibility(View.GONE);


//        if (mOpenCvCameraView != null) {
//            mOpenCvCameraView.disableView();
//        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.main_surface_view);

        mOpenCvCameraView = (CustomSufaceView) findViewById(R.id.main_surface_view);
        mOpenCvCameraView.setCvCameraViewListener(this);
        imageCapture = (ImageView) findViewById(R.id.imageCapture);

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        loadOpenCVLibrary();

    }

    private void loadOpenCVLibrary() {
        if (!OpenCVLoader.initDebug()) {
            Log.d("OpenCV", "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            boolean success = OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_1_0, this, mLoaderCallback);
            if (!success)
                Log.e("OpenCV", "Asynchronous initialization failed!");
            else
                Log.d("OpenCV", "Asynchronous initialization succeeded!");
        } else {
            Log.d("OpenCV", "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mRgba = new Mat();

        setCameraResolution(6);
        setCameraParameters();
        initializeCameraVariables(width, height);
    }

    private void setCameraResolution(int typeOfResolution) {
        List<Camera.Size> mResolutionList = mOpenCvCameraView.getResolutionList();
        Camera.Size resolution = mResolutionList.get(typeOfResolution);
        mOpenCvCameraView.setResolution(resolution);
    }

    private void setCameraParameters() {
        Camera.Parameters cParams = mOpenCvCameraView.getParameters();
        cParams.setFocusMode(Camera.Parameters.FOCUS_MODE_INFINITY);
        cParams.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        mOpenCvCameraView.setParameters(cParams);
    }

    private void initializeCameraVariables(int width, int height) {
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mColorBlobDetector = new ColorBlobDetector();
        mSpectrum = new Mat();
        SPECTRUM_SIZE = new Size(200, 64);
        CONTOUR_COLOR = new Scalar(87, 33, 185, 255);
    }

    @Override
    public void onCameraViewStopped() {

//        Camera.Parameters cParams = mOpenCvCameraView.getParameters();
//        cParams.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
//        mOpenCvCameraView.setParameters(cParams);
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }
        mRgba.release();
        mSpectrum.release();

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

//        setDetectPoint();

        return false; // don't need subsequent touch events
    }

    private boolean setDetectPoint() {
        Rect touchedRect = calculateTouchRegion();
        if (touchedRect == null) return false;
        Mat touchedRegionRgba = mRgba.submat(touchedRect);
        Mat touchedRegionHsv = new Mat();

        Imgproc.cvtColor(touchedRegionRgba, touchedRegionHsv, Imgproc.COLOR_RGB2HSV_FULL);

        calculateAverageColorOfTouch(touchedRegionHsv, touchedRect);

        mIsColorSelected = !mIsColorSelected;

        touchedRegionRgba.release();
        touchedRegionHsv.release();
        return true;
    }

    private Rect calculateTouchRegion() {
        int cols = mRgba.cols();
        int rows = mRgba.rows();
        int x = mOpenCvCameraView.getWidth() / 2;
        int y = mOpenCvCameraView.getHeight() / 2;

        Log.i(TAG, "Touch image coordinates: (" + x + ", " + y + ")");

        if ((x < 0) || (y < 0) || (x > cols) || (y > rows)) return null;

        Rect touchedRect = new Rect();

        touchedRect.x = (x > 5) ? x - 5 : 0;
        touchedRect.y = (y > 5) ? y - 5 : 0;

        touchedRect.width = (x + 5 < cols) ? x + 5 - touchedRect.x : cols - touchedRect.x;
        touchedRect.height = (y + 5 < rows) ? y + 5 - touchedRect.y : rows - touchedRect.y;

        return touchedRect;
    }

    private void calculateAverageColorOfTouch(Mat touchedRegionHsv, Rect touchedRect) {
        Scalar mBlobColorHsv = Core.sumElems(touchedRegionHsv);
        int pointCount = touchedRect.width * touchedRect.height;
        for (int i = 0; i < mBlobColorHsv.val.length; i++) {
            mBlobColorHsv.val[i] /= pointCount;
        }

        mColorBlobDetector.setHsvColor(mBlobColorHsv);

        Imgproc.resize(mColorBlobDetector.getSpectrum(), mSpectrum, SPECTRUM_SIZE);
    }

    private Scalar converScalarHsv2Rgba(Scalar hsvColor) {
        Mat pointMatRgba = new Mat();
        Mat pointMatHsv = new Mat(1, 1, CvType.CV_8UC3, hsvColor);
        Imgproc.cvtColor(pointMatHsv, pointMatRgba, Imgproc.COLOR_HSV2RGB_FULL, 4);

        return new Scalar(pointMatRgba.get(0, 0));
    }

    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        if (isPhotoMaked) return mRgba;


        if (!mIsColorSelected) return mRgba;

        List<MatOfPoint> contours = mColorBlobDetector.getContours();
        mColorBlobDetector.process(mRgba);

        Log.d(TAG, "Contours count: " + contours.size());

        if (contours.size() <= 0) {
            return mRgba;
        }

        RotatedRect rect = Imgproc.minAreaRect(new MatOfPoint2f(contours.get(0).toArray()));

        double boundWidth = rect.size.width;
        double boundHeight = rect.size.height;
        int boundPos = 0;

        for (int i = 1; i < contours.size(); i++) {
            rect = Imgproc.minAreaRect(new MatOfPoint2f(contours.get(i).toArray()));
            if (rect.size.width * rect.size.height > boundWidth * boundHeight) {
                boundWidth = rect.size.width;
                boundHeight = rect.size.height;
                boundPos = i;
            }
        }

        Rect boundRect = Imgproc.boundingRect(new MatOfPoint(contours.get(boundPos).toArray()));

//        Imgproc.rectangle( mRgba, boundRect.tl(), boundRect.br(), CONTOUR_COLOR_WHITE, 2, 8, 0 );


        Log.d(TAG,
                " Row start [" +
                        (int) boundRect.tl().y + "] row end [" +
                        (int) boundRect.br().y + "] Col start [" +
                        (int) boundRect.tl().x + "] Col end [" +
                        (int) boundRect.br().x + "]");

        double a = boundRect.br().y - boundRect.tl().y;
        a = a * 0.7;
        a = boundRect.tl().y + a;

        Log.d(TAG,
                " A [" + a + "] br y - tl y = [" + (boundRect.br().y - boundRect.tl().y) + "]");

//        Imgproc.rectangle( mRgba, boundRect.tl(), new Point(boundRect.br().x, a), CONTOUR_COLOR, 2, 8, 0 );

        MatOfPoint2f pointMat = new MatOfPoint2f();
        Imgproc.approxPolyDP(new MatOfPoint2f(contours.get(boundPos).toArray()), pointMat, 3, true);
        contours.set(boundPos, new MatOfPoint(pointMat.toArray()));

        MatOfInt hull = new MatOfInt();
        MatOfInt4 convexDefect = new MatOfInt4();
        Imgproc.convexHull(new MatOfPoint(contours.get(boundPos).toArray()), hull);

        if (hull.toArray().length < 3) return mRgba;

        Imgproc.convexityDefects(new MatOfPoint(contours.get(boundPos).toArray()), hull, convexDefect);

        List<MatOfPoint> hullPoints = new LinkedList<MatOfPoint>();
        List<Point> listContourPoints = new LinkedList<Point>();
        for (int j = 0; j < hull.toList().size(); j++) {
            listContourPoints.add(contours.get(boundPos).toList().get(hull.toList().get(j)));
        }

        MatOfPoint e = new MatOfPoint();
        e.fromList(listContourPoints);
        hullPoints.add(e);
//        double iThreshold = 3000; good value
        double iThreshold = 9000;


//        List<MatOfPoint> defectPoints = new LinkedList<MatOfPoint>();
        List<Point> listPoDefect = new LinkedList<Point>();
        for (int j = 0; j < convexDefect.toList().size(); j = j + 4) {
            Point farPoint = contours.get(boundPos).toList().get(convexDefect.toList().get(j + 2));
            Integer depth = convexDefect.toList().get(j + 3);
            if (depth > iThreshold && farPoint.y < a) {
                listPoDefect.add(contours.get(boundPos).toList().get(convexDefect.toList().get(j + 2)));
            }
            Log.d(TAG, "defects [" + j + "] " + convexDefect.toList().get(j + 3));
        }

        MatOfPoint e2 = new MatOfPoint();
        e2.fromList(listPoDefect);
//        defectPoints.add(e2);
//        hullPoints.add(e2);
        Log.d(TAG, "hull: " + hull.toList());
        Log.d(TAG, "defects: " + convexDefect.toList());

        Imgproc.drawContours(mRgba, hullPoints, -1, CONTOUR_COLOR, 10);

        int defectsTotal = (int) convexDefect.total();
        Log.d(TAG, "Defect total " + defectsTotal);


        if (getNumberOfFingers(listPoDefect) == 5) {

            mHandler.post(mUpdateFingerCountResults);
            isPhotoMaked = true;
            makeCapture();


//            mHandler.post(mUpdateFingerCountResults);
        }

        for (Point p : listPoDefect) {
            Imgproc.circle(mRgba, p, 10, new Scalar(255, 0, 255));
        }

        return mRgba;
    }

    private void manageBlinkEffect() {
        ObjectAnimator anim = ObjectAnimator.ofInt(mOpenCvCameraView, "backgroundColor", Color.WHITE,
                Color.alpha(123));
        anim.setDuration(130);
        anim.setEvaluator(new ArgbEvaluator());
//        anim.setRepeatMode(ValueAnimator.RESTART);
//        anim.setAutoCancel(true);
        anim.start();
    }


    final Runnable mUpdateFingerCountResults = new Runnable() {
        public void run() {
            try {
                updateNumberOfFingers();
            } catch (IOException e) {
                e.printStackTrace();
            }
//            manageBlinkEffect();
        }
    };

    private int getNumberOfFingers(List<Point> listPoDefect) {
        numberOfFingers = listPoDefect.size();
        if (numberOfFingers > 5) {
            numberOfFingers = 5;
        }
        return numberOfFingers;
    }

    private void updateNumberOfFingers() throws IOException {
        Toast.makeText(this, "5 Fingers", Toast.LENGTH_SHORT).show();
        savebitmap(capturedPhoto);
        imageCapture.setVisibility(View.VISIBLE);
        imageCapture.setImageBitmap(capturedPhoto);
        imageCapture.invalidate();
    }

    public static File savebitmap(Bitmap bmp) throws IOException {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 60, bytes);
        File f = new File(Environment.getExternalStorageDirectory()
                + File.separator + "testimage.jpg");
        f.createNewFile();
        FileOutputStream fo = new FileOutputStream(f);
        fo.write(bytes.toByteArray());
        fo.close();
        return f;
    }
}
